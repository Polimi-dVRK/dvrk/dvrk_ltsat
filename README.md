# LTSAT

This repository contains an implementation of **L**ong **T**erm **S**afety **A**rea **T**racking, a technique developped by [Penza et al.](https://doi.org/10.3389/frobt.2017.00015) as part of the EnViSoRS framework.

The technique enables robust long term tracking of features within a defined safety area even if the area is partially or totally occluded. In addition, the method is robust to disturbances such as smoke obscuring the image.


