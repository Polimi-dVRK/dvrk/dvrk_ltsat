
/**
 * @file
 *
 * Implement the required image processing steps to obtain rectified stereo images apt for use in the stereo
 * reconstruction phase of the algorithm.
 */

#include <ros/ros.h>
#include <nodelet/loader.h>

#include <dvrk_common/ros/ext.hpp>
#include <dvrk_common/ros/params.hpp>
#include <dvrk_common/ros/advertisement_checker.hpp>

void setup_image_nodelets(const std::string& ns, nodelet::Loader &nodelet) {
    nodelet::M_string remap(ros::names::getRemappings());
    nodelet::V_string nodelet_argv;
    
    ros::NodeHandle private_nh("~");
    XmlRpc::XmlRpcValue image_proc_params;
    
    if (const auto queue = dvrk::getParam<int>(private_nh, "queue_size"))
        image_proc_params["queue_size"] = *queue;
    
    // This utility function simply moves a name into the specified namespace.
    // It is used when remapping nodelet IOs to ensure that they go the right namespaces
    auto mkname = [&ns](const std::string& name) -> std::string {
        return ros::names::append(ros::this_node::getNamespace(), ns, name);
    };
    
    
    // Step 0: Region of Interest
    remap["camera/image_raw"] = ros::names::append(
        ros::this_node::getNamespace(), "raw", ns, "image_raw");
    remap["camera_out/image_raw"] = mkname("image_raw");
    
    std::string cropper_name = ns + "_cropper";
    ros::param::set(cropper_name, image_proc_params);
    
    ROS_INFO_STREAM(
        "" << "Creating \"image_proc/crop_decimate\" as <" << cropper_name <<  "> using:\n"
           << "  Input: " << remap["camera/image_raw"] << "\n"
           << "  Output: " << remap["camera_out/image_raw"]
    );
    
    nodelet.load(cropper_name, "image_proc/crop_decimate", remap, nodelet_argv);
    
    // Step 1: Debayering
    remap["image_raw"] = mkname("image_raw");
    remap["image_mono"] = mkname("image_mono");
    remap["image_color"] = mkname("image_color");
    
    std::string debayer_name = ns + "_debayer";
    ros::param::set(debayer_name, image_proc_params);

    ROS_INFO_STREAM(
        "" << "Creating \"image_proc/debayer\" as <" << debayer_name << "> node using:\n"
           << "  Input: " << remap["image_raw"] << "\n" 
           << "  Output: " << remap["image_mono"] << "\n"
           << "  Output: " << remap["image_color"]
    );
        
    nodelet.load(debayer_name, "image_proc/debayer", remap, nodelet_argv);
    
    // Step 2: Rectify mono images
    remap["image_mono"] = mkname("image_mono");
    remap["image_rect"] = mkname("image_rect");
    
    std::string rectify_mono_name = ns + "_rectify_mono";
    ros::param::set(rectify_mono_name, image_proc_params);
    
    ROS_INFO_STREAM(
        "" << "Creating \"image_proc/rectify\" as <" << rectify_mono_name << "> node using:\n"
           << "  Input: " << remap["image_mono"] << "\n"
           << "  Output: " << remap["image_rect"]
    );
    
    nodelet.load(rectify_mono_name,  "image_proc/rectify", remap, nodelet_argv);
    
    // Step 3: Rectify colour images
    remap["image_mono"] = mkname("image_color");
    remap["image_rect"] = mkname("image_rect_color");
    
    std::string rectify_color_name = ns + "_rectify_colour";
    ros::param::set(rectify_color_name, image_proc_params);
    
    ROS_INFO_STREAM(
        "" << "Creating \"image_proc/rectify\" as <" << rectify_color_name << "> node using:\n"
           << "  Input: " << remap["image_mono"] << "\n"
           << "  Output: " << remap["image_rect"]
    );
    
    nodelet.load(rectify_color_name, "image_proc/rectify", remap, nodelet_argv);
}

int main(int argc, char** argv) {
    
    ros::init(argc, argv, "ltsat");
    
    // We need to create a bunch of nodelets for the image processing pipeline.
    //
    // Each camera image should go through the following steps:
    //     - convert the raw image to grayscale (debayering node) (image_raw -> image_mono, image_color)
    //     - rectify the mono image (image_mono -> image_rect_)
    //     - rectify the color image (image_color -> image_rect_color)
    //
    
    nodelet::Loader nodelet(false); // Don't bring up  the manager ROS API
    setup_image_nodelets("left", nodelet);
    setup_image_nodelets("right", nodelet);
    
    // Set-up the stereo nodelet
    nodelet::M_string remap_stereo(ros::names::getRemappings());
    nodelet::V_string nodelet_stereo_argv;

    nodelet.load("elas_stereo", "dvrk_stereo/elas_stereo", remap_stereo, nodelet_stereo_argv);
    
    // Finally, make sure that all the required topics are publishing
    dvrk::AdvertisementChecker checker;
    checker.check({
        ros::names::resolve("left/image_raw"),
        ros::names::resolve("left/camera_info"),
        ros::names::resolve("right/image_raw"),
        ros::names::resolve("right/camera_info")
    });

    ros::spin();

    return 0;
}
